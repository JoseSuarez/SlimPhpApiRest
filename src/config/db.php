<?php
class db{
    private $dbHost = 'localhost';
    private $dbUser = 'root';
    private $dbPass = '';
    private $dbName = 'php_apiRest';
    //coneccion
    public function conectDB(){
        $mysqlConect = "mysql:host=$this->dbHost;dbname=$this->dbName";
        $dbConection = new PDO( $mysqlConect, $this->dbUser, $this->dbPass );
        $dbConection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbConection;
    }
}