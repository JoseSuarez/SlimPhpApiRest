<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

$app = AppFactory::create();

$app->get('/api/clientes', function( Request $request, Response $response ){
    $sql = "SELECT * FROM clientes";
    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->query($sql);

        if ($result->rowCount() > 0) {
            $clientes = $result->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($clientes));
            return $response;
        } else {
            $response->getBody()->write(json_encode("No existen clientes en la BBDD."));
            return $response;
        }
        $result = null;
        $db = null;

    } catch (PDOException $e) {
        echo '{"error": {"text": '. $e->getMessage().'}}';
    }
});

// Recuperar cliente por ID
$app->get('/api/clientes/{id}', function( Request $request, Response $response ){
    $id_cliente = $request->getAttribute('id'); 
    $sql = "SELECT * FROM clientes WHERE id = $id_cliente";
    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->query($sql);

        if ($result->rowCount() > 0) {
            $cliente = $result->fetchAll(PDO::FETCH_OBJ);
            $response->getBody()->write(json_encode($cliente));
            return $response;
        } else {
            $response->getBody()->write(json_encode("No existe cliente en la BBDD con ese ID."));
            return $response;
        }
        $result = null;
        $db = null;

    } catch (PDOException $e) {
        echo '{"error": {"text": '. $e->getMessage().'}}';
    }
});

// Crear nuevo cliente
$app->post('/api/clientes', function( Request $request, Response $response ){

    $data = json_decode($request->getBody(), true  );
    $nombre     = $data['nombre'];
    $apellidos  = $data['apellidos'];
    $telefono   = $data['telefono'];
    $email      = $data['email'];
    $direccion  = $data['direccion'];
    $ciudad     = $data['ciudad'];

    $sql = "INSERT INTO clientes (nombre, apellidos, telefono, email, direccion, ciudad) VALUES 
            (:nombre, :apellidos, :telefono, :email, :direccion, :ciudad)";

    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->prepare($sql);

        $result->bindParam(':nombre', $nombre);
        $result->bindParam(':apellidos', $apellidos);
        $result->bindParam(':telefono', $telefono);
        $result->bindParam(':email', $email);
        $result->bindParam(':direccion', $direccion);
        $result->bindParam(':ciudad', $ciudad);

        $result->execute();
        $response->getBody()->write(json_encode("Nuevo cliente guardado."));
        return $response;

        $result = null;
        $db = null;

    } catch (PDOException $e) {
        $response->getBody()->write('{"error": {"text": '. $e->getMessage().'}}');
        return $response;
    }
});

// Modificar cliente
$app->put('/api/clientes/{id}', function( Request $request, Response $response ){

    $data = json_decode($request->getBody(), true  );
    $id_cliente = $request->getAttribute('id');
    $nombre     = $data['nombre'];
    $apellidos  = $data['apellidos'];
    $telefono   = $data['telefono'];
    $email      = $data['email'];
    $direccion  = $data['direccion'];
    $ciudad     = $data['ciudad'];

    $sql = "UPDATE clientes SET 
             nombre = :nombre, 
             apellidos= :apellidos, 
             telefono= :telefono,
             email= :email, 
             direccion= :direccion, 
             ciudad= :ciudad
            WHERE id = $id_cliente";

    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->prepare($sql);

        $result->bindParam(':nombre', $nombre);
        $result->bindParam(':apellidos', $apellidos);
        $result->bindParam(':telefono', $telefono);
        $result->bindParam(':email', $email);
        $result->bindParam(':direccion', $direccion);
        $result->bindParam(':ciudad', $ciudad);

        $result->execute();
        $response->getBody()->write(json_encode("Cliente modificado."));
        return $response;

        $result = null;
        $db = null;

    } catch (PDOException $e) {
        $response->getBody()->write('{"error": {"text": '. $e->getMessage().'}}');
        return $response;
    }
});

// Borrar cliente
$app->delete('/api/clientes/{id}', function( Request $request, Response $response ){

    $id_cliente = $request->getAttribute('id');

    $sql = "DELETE FROM clientes WHERE id = $id_cliente";

    try {
        $db = new db();
        $db = $db->conectDB();
        $result = $db->prepare($sql);
        $result->execute();

        if ($result->rowCount() > 0) {
            $response->getBody()->write(json_encode("Cliente eliminado."));
            return $response;
        } else {
            $response->getBody()->write(json_encode("No existe cliente con ese ID."));
            return $response;
        };

        $result = null;
        $db = null;

    } catch (PDOException $e) {
        $response->getBody()->write('{"error": {"text": '. $e->getMessage().'}}');
        return $response;
    }
});